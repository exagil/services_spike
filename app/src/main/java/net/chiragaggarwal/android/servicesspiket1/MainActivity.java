package net.chiragaggarwal.android.servicesspiket1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button buttonEndService;
    private Button buttonStartService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeWidgets();
        setClickEventListeners();
        startService(new Intent(this, BackgroundService.class));
    }

    private void initializeWidgets() {
        this.buttonStartService = (Button) findViewById(R.id.button_start_service);
        this.buttonEndService = (Button) findViewById(R.id.button_start_service);
    }

    private void setClickEventListeners() {
        setOnClickListenerForStartServiceButton();
        setOnClickListenerForEndServiceButton();
    }

    private void setOnClickListenerForStartServiceButton() {
        this.buttonStartService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(new Intent(MainActivity.this, BackgroundService.class));
            }
        });
    }

    private void setOnClickListenerForEndServiceButton() {
        this.buttonEndService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(MainActivity.this, BackgroundService.class));
            }
        });
    }
}
