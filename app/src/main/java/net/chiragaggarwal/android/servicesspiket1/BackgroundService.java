package net.chiragaggarwal.android.servicesspiket1;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class BackgroundService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("chi6rag", "onStartCommand");
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i("chi6rag", "onDestroy");
    }

    @Override
    public void onCreate() {
        Log.i("chi6rag", "onCreate");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i("chi6rag", "onBind");
        return null;
    }
}
